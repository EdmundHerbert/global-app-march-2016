angular.module('globalApp.services', [])

    // Http calls to server
    .factory('httpService', function ($http) {
        var httpService = {};

        httpService.getUser = function (dataIn, method) {
            //return $http.post('https://dpdev.co.za:8181/HaloMobile/MobileAJAXServer?method=' + method, dataIn).then(function (res) { // changed on 20160229 from GlabalApp 
            return $http.post('https://globalapp.co.za:8181/HaloMobile/MobileAJAXServer?method=' + method, dataIn).then(function (res) { // changed on 20160229 from GlabalApp/changed on 20160318 back to globalapp
                return res.data;
            }, function (error) {
                alert(error.message);
                return [];
            });
        };
        return httpService;
    })
    // Http Extended calls to server pre login
    .factory('httpServiceSecond', function ($http) {

        var httpServiceSecond = {};

        httpServiceSecond.getUser = function (dataIn, method) {
            //return $http.post('https://dpdev.co.za:8181/HaloMobile/MobileAJAX?method=' + method, dataIn).then(function (res) { // implemented on 20160301

            //return $http.post('https://dpdev.co.za:8181/HaloMobile/MobileAJAXPreLogin?method=' + method, dataIn).then(function (res) { // implemented on 20160301 //changed on 20160316 as per mikes so called verbal note

            return $http.post('https://globalapp.co.za:8181/HaloMobile/MobileAJAXServer?method=' + method, dataIn).then(function (res) { // implemented on 20160301 // changed on 20160316 as per mikes so called verbal note/changed on 20160318 until further notice issues sesseion, encryption etc

                return res.data;
            }, function (error) {
                alert(error.message);
                return [];
            });
        };
        return httpServiceSecond;
    })
    // Http Extended calls to server post login
    .factory('httpServiceThird', function ($http) {

        var httpServiceThird = {};

        httpServiceThird.getUser = function (dataIn, method) {

            return $http.post('https://dpdev.co.za:8181/HaloMobile/MobileAJAX?method=' + method, dataIn).then(function (res) { // implemented on 20160301

                // return $http.post('https://dpdev.co.za:8181/HaloMobile/MobileAJAXPreLogin?method=' + method, dataIn).then(function (res) { // implemented on 20160301 // changed on 20160316 as per mikes so called verbal note

                return res.data;
            }, function (error) {
                alert(error.message);
                return [];
            });
        };
        return httpServiceThird;
    })

    // Show Toast Message
    //.factory('toast', function($rootScope, $timeout, $ionicPopup, $cordovaToast) {
    //    return {
    //        show: function(message, duration, position) {
    //            message = message || "There was a problem...";
    //            duration = duration || 'short';
    //            position = position || 'top';

    //            if (!!window.cordova) {
    //                // Use the Cordova Toast plugin
    //                $cordovaToast.show(message, duration, position);
    //            } else {
    //                if (duration === 'short') {
    //                    duration = 2000;
    //                } else {
    //                    duration = 5000;
    //                }

    //                var myPopup = $ionicPopup.show({
    //                    template: "<div class='toast'>" + message + "</div>",
    //                    scope: $rootScope,
    //                    buttons: []
    //                });

    //                $timeout(function() {
    //                    myPopup.close();
    //                }, duration);
    //            }
    //        }
    //    };
    //})


    // DBA Function for SQLite
    .factory('dBa', function ($cordovaSQLite, $q, $ionicPlatform) {
        var self = this;
        //Handle query's and potential errors
        self.query = function (query, parameters) {
            parameters = parameters || [];
            var q = $q.defer();

            $ionicPlatform.ready(function () {
                $cordovaSQLite.execute(db, query, parameters)
                    .then(function (result) {
                        q.resolve(result);
                    }, function (error) {
                        alert('I found an error: ' + error.message);
                        q.reject(error);
                    });
            });
            return q.promise;
        };

        //Process a result set
        self.getAll = function (result) {
            var output = [];

            for (var i = 0; i < result.rows.length; i++) {
                output.push(result.rows.item(i));
            }
            return output;
        };

        //Process a single result
        self.getById = function (result) {
            var output = angular.copy(result.rows.item(0));
            return output;
        };
        return self;
    })

    // Member Data
    .factory("member", function (dBa) {
        var self = this;
        self.all = function () {
            return dBa.query("SELECT memberID FROM member")
                .then(function (result) {
                    return dBa.getAll(result);
                }, function (error) { alert(error.message); });
        };
        self.add = function (memberid, membertoken) {
            var parameters = [memberid, membertoken];
            return dBa.query("INSERT INTO member (memberID, memberToken) VALUES (?,?)", parameters);
        }, function (error) { alert(error.message); };
        return self;
    })

    // ClientSelected Data
    .factory("clientSelected", function ($localStorage, dBa, client) {
        var self = this;
        self.all = function () {
            return dBa.query("SELECT * FROM clientSelected")
                .then(function (result) {
                    return dBa.getAll(result);
                }, function (error) { alert("All ClientSelected: " + error.message); });
        }, function (error) { alert("All ClientSelected: " + error.message); };

        self.add = function (selected, clientName) {
            var parameters = [selected, clientName];
            // Get ClientID
            client.single(clientName).then(function (result) {
                $localStorage.clientId = result.clientID;
            }, function (error) { alert("Client Single in ClientSelected: " + error.message); });
            return dBa.query("INSERT INTO clientSelected (selected, clientName) VALUES (?,?)", parameters);
        }, function (error) { alert("Client Selected: " + error.message); };
        self.count = function () {
            return dBa.query("SELECT Count(id) AS count FROM clientSelected");
        }, function (error) { alert(error.message); };
        self.remove = function (product) {
            var parameters = [product.clientName];
            return dBa.query("DELETE FROM clientSelected WHERE clientName = (?)", parameters);
        }, function (error) { alert(error.message); };
        self.drop = function () {
            return dBa.query('DROP TABLE clientSelected');
        }, function (error) { alert(error.message); };
        self.create = function () {
            return dBa.query("CREATE TABLE IF NOT EXISTS clientSelected (id integer primary key,selected text,clientName text)");
        }, function (error) { alert(error.message); };
        self.change = function (clientName, clientSelected) {
            var clientParameters = [clientName, clientSelected];
            client.single(clientName).then(function (result) {
                $localStorage.clientId = result.clientID;
            }, function (error) { alert("Client Single in Client Selected: " + error.message); });
            return dBa.query("UPDATE clientSelected SET clientName = ?,selected = ?", clientParameters);

        }
        return self;
    })

    //Product Data
    .factory("product", function (dBa) {
        var self = this;
        self.all = function () {
            return dBa.query("SELECT productID,productName,productIcon,productClientId FROM products")
                .then(function (resultAll) {
                    return dBa.getAll(resultAll);
                }, function (error) { alert(error.message); });
        };
        self.many = function (productClientId) {
            var parametersProduct = [productClientId];
            return dBa.query("SELECT productID,productName,productIcon,productClientId FROM products WHERE productClientId = (?)", parametersProduct)
                .then(function (result) {
                    return dBa.getAll(result);
                }, function (error) { alert(error.message); });
        };
        self.productName = function (inproductName) {
            var parameters = [inproductName];
            return dBa.query("SELECT productID,productName,productIcon FROM products WHERE productName = (?)", parameters)
                .then(function (result) {
                    return dBa.getById(result);
                }, function (error) { alert(error.message); });
        }, function (error) { alert(error.message); };
        self.single = function (clientId) {
            var parameters = [clientId];
            return dBa.query("SELECT productID,productName,productIcon FROM products WHERE productClientId = (?)", parameters)
                .then(function (result) {
                    return dBa.getById(result);
                }, function (error) { alert(error.message); });
        }, function (error) { alert(error.message); };

        self.add = function (product, client) {
            var parameters = [product.productID, product.productName, product.productIcon, client];
            return dBa.query("INSERT INTO products (productID,productName,productIcon,productClientId) VALUES (?,?,?,?)", parameters);
        }, function (error) { alert(error.message); };

        self.remove = function (product) {
            var parameters = [product.productID];
            return dBa.query("DELETE FROM products WHERE productID = (?)", parameters);
        }, function (error) { alert(error.message); };

        self.count = function () {
            return dBa.query("SELECT Count(id) AS count FROM products");
        }, function (error) { alert(error); };

        self.drop = function () {
            return dBa.query("DROP TABLE PRODUCTS");
        }, function (error) { alert(error.message); };
        self.create = function () {
            return dBa.query("CREATE TABLE IF NOT EXISTS products (id integer primary key,productID text,productName text,productIcon text,productClientId text)");
        }, function (error) { alert(error.message); }
        return self;
    })

    // Client Data
    .factory("client", function ($localStorage, dBa, product) {
        var self = this;
        self.all = function () {
            return dBa.query("SELECT * FROM client")
                .then(function (result) {
                    return dBa.getAll(result);
                }, function (error) { alert(error.message); });
        }, function (error) { alert("Client All: " + error.message); };

        self.add = function (client) {
            $localStorage.clientId = client.clientID;
            var parameters = [client.clientID, client.clientCode, client.clientName];
            return dBa.query("INSERT INTO client (clientID, clientCode,clientName) VALUES (?,?,?)", parameters);
        }, function (error) { alert("Client Add: " + error.message); };

        self.remove = function (client) {
            var parameters = [client.clientID];
            return dBa.query("DELETE FROM client WHERE clientID = (?)", parameters);
        }, function (error) { alert("Client Remove: " + error.message); };

        self.count = function () {
            return dBa.query("SELECT Count(id) AS count FROM client");
        }, function (error) { alert("Client Count: " + error.message); };

        self.single = function (clientName) {
            //alert("ClientName: " + clientName);
            var parameters = [clientName];
            return dBa.query("SELECT clientID FROM client WHERE clientName = (?)", parameters)
            .then(function (result) {
                return dBa.getById(result);
            }, function (error) { alert("Client Single in Client: " + error.message); });
        }, function (error) { alert("Client Single in Client: " + error.message); };

        self.drop = function () {
            return dBa.query("DROP TABLE CLIENT");
        }, function (error) { alert("Client Drop: " + error.message); };

        self.create = function () {
            return dBa.query("CREATE TABLE IF NOT EXISTS client (id integer primary key,clientID text,clientCode text,clientName text)");
        }, function (error) { alert("Create Table Client: " + error.message); };

        self.testcount = function (dataHttpIn) {
            self.drop().then(function () {
                product.drop().then(function () {
                    self.create().then(function () {
                        product.create().then(function () {
                            function wait() {
                                for (var x = 0; x < dataHttpIn.clients.length; x++) {
                                    self.add(dataHttpIn.clients[x]).then(function () { });
                                    var clientid = dataHttpIn.clients[x].clientID;
                                    console.log("Client Add: " + dataHttpIn.clients[x].clientName);

                                    for (var k = 0; k < dataHttpIn.clients[x].products.length; k++) {
                                        var pr1 = dataHttpIn.clients[x].products[k];
                                        product.add(pr1, clientid).then(function () {
                                        }, function (error) {
                                            console.log(error.message);
                                        });
                                        console.log("Product Add: " + dataHttpIn.clients[x].products[k].productName);
                                    }
                                }
                            } setTimeout(wait, 1500);
                        }, function (error) { alert("Client TestCount: " + error.message); });
                    }, function (error) { alert("Client TestCount: " + error.message); });
                }, function (error) { alert("ClientTestCount" + error.message); });
            }, function (error) { alert("Client TestCount: " + error.message); });
        }, function (error) { alert("Client TestCount: " + error.message); };
        return self;
    })


    //Vehicle Licence Data
    .factory("vehicleLicence", function (dBa, httpService, $localStorage) {
        var self = this;
        self.all = function () {
            return dBa.query("SELECT memberID,dependantID,controlNumber,licenceNumber,registerNumber,vehicleDescription,vehicleMake,seriesName,vehicleColour,vinNumber," +
                    "engineNumber,expiryDate,vehicleYear FROM vehiclelicence")
                .then(function (result) {
                    return dBa.getAll(result);
                }, function (error) { alert(error.message); });
        }, function (error) { alert(error.message); };

        self.add = function (vehicle) {
            var parameters = [
                vehicle[0].memberID, vehicle[0].dependantID, vehicle[0].controlNumber, vehicle[0].licenceNumber, vehicle[0].registerNumber, vehicle[0].vehicleDescription, vehicle[0].vehicleMake, vehicle[0].seriesName,
                vehicle[0].vehicleColour, vehicle[0].vinNumber, vehicle[0].engineNumber, vehicle[0].expiryDate, vehicle[0].vehicleYear
            ];
            return dBa.query("INSERT INTO vehiclelicence (memberID,dependantID,controlNumber,licenceNumber,registerNumber,vehicleDescription,vehicleMake,seriesName,vehicleColour,vinNumber," +
                "engineNumber,expiryDate,vehicleYear) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)", parameters);
        }, function (error) { alert(error.message); };

        self.remove = function (vehicle) {
            var parameters = [vehicle.memberID];
            return dBa.query("DELETE FROM vehiclelicence WHERE memberID = (?)", parameters);
        }, function (error) { alert(error.message); };

        self.count = function () {
            return dBa.query("SELECT Count(id) AS count FROM vehiclelicence");
        }, function (error) { alert(error.message); };

        self.getvehicledata = function () {

            // Input locally stored memberID
            var datamemberVehicleLicence = {
                memberID: $localStorage.memberId
            };

            var vehicleDetailsIn = null;
            var methodVehicleLicence = "MobileGetVehicleLicenceInfo";

            // Get Vehicle data
            httpService.getUser(datamemberVehicleLicence, methodVehicleLicence).then(function (data) {
                vehicleDetailsIn = data;
                self.count().then(function (result) {
                    if (result.rows.item(0).count < 1) { // Add vehicle data if database empty
                        self.add(vehicleDetailsIn).then(function () { });
                        // console.log('Adding Vehicle Data ');
                    }
                });
            }, function (error) { alert(error.message); });
        };
        return self;
    })

    // terms and conditions
    .factory("termsConditions", function (dBa) {
        var self = this;
        self.all = function () {
            return dBa.query("SELECT * FROM termsConditions")
                .then(function (result) {
                    return dBa.getAll(result);
                }, function (error) { alert(error.message); });
        }, function (error) { alert(error.message); };

        self.count = function () {
            return dBa.query("SELECT Count(id) AS count FROM termsConditions");
        }, function (error) { alert(error.message); };
        self.add = function (termsDate) {
            var parameters = [termsDate];
            return dBa.query("INSERT INTO termsConditions (dateAccepted) VALUES (?)", parameters);
        }, function (error) { alert(error.message); };
        return self;
    })


    // Personal Info Data
    .factory("personalInfo", function ($q, dBa,$http) {
        var self = this;
        self.all = function () {
                 return dBa.query("SELECT * FROM personalinfo")
                .then(function (result) {
                    return dBa.getAll(result);
                }, function (error) { alert(error.message); });
        }, function (error) { alert(error.message); };

        self.add = function (personalinfodata) {
            var parameters = [
                personalinfodata.serverID, personalinfodata.name, personalinfodata.surname, personalinfodata.idNumber, personalinfodata.emailAddress, personalinfodata.language, personalinfodata.gender, personalinfodata.idType, personalinfodata.homePhoneNumber,
                personalinfodata.workPhoneNumber, personalinfodata.medicalAidName, personalinfodata.medicalAidNumber, personalinfodata.homeAddress, personalinfodata.medicalAidScheme, personalinfodata.workAddress,
                personalinfodata.emergencyContactName1, personalinfodata.emergencyContactNumber1, personalinfodata.emergencyContactName2, personalinfodata.emergencyContactNumber2,
                personalinfodata.occupation, personalinfodata.businessName, personalinfodata.bloodType, personalinfodata.bloodDonorNumber, personalinfodata.allergies, personalinfodata.homeLatitude, personalinfodata.homeLongitude, personalinfodata.imageString,personalinfodata.dateOfBirth
            ];
            return dBa.query("INSERT INTO personalinfo (serverID,name,surname,idNumber,emailAddress,language,gender,idType,homePhoneNumber,workPhoneNumber,medicalAidName,medicalAidNumber,homeAddress," +
                "medicalAidScheme,workAddress,emergencyContactName1,emergencyContactNumber1,emergencyContactName2,emergencyContactNumber2,occupation,businessName," +
                "bloodType,bloodDonorNumber,allergies,homeLatitude,homeLongitude,imagePath,birthdate) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)", parameters);
        }, function (error) { alert(error.message); };

        self.remove = function (personalinfo) {
            var parameters = [personalinfo.serverID];
            return dBa.query("DELETE FROM personalinfo WHERE serverID = (?)", parameters);
        }, function (error) { alert(error.message); };

        self.count = function () {
            return dBa.query("SELECT Count(id) AS count FROM personalinfo");
        }, function (error) { alert(error.message); };

        self.addData = function (dataHttpPersonalInfoIn) {
            self.count().then(function (result) {
                if (result.rows.item(0).count < 1) {
                    self.add(dataHttpPersonalInfoIn);
                }
            }, function (error) { alert(error.message); });
        }, function (error) { alert(error.message); };

        self.drop = function () {
            return dBa.query("DROP TABLE personalinfo");
        }, function (error) { alert(error.message); };

        self.create = function () {
            return dBa.query("CREATE TABLE IF NOT EXISTS personalinfo (id integer primary key,serverID text,name text, surname text,idNumber text,emailAddress text"
                + ",language text,gender text,idType text,homePhoneNumber text,workPhoneNumber text,medicalAidName text" +
                ",medicalAidNumber text,homeAddress text,medicalAidScheme text,workAddress text,emergencyContactName1 text"
                + ",emergencyContactNumber1 text,emergencyContactName2 text,emergencyContactNumber2 text,occupation text,businessName text,bloodType text" +
                ",bloodDonorNumber text,allergies text,homeLatitude text,homeLongitude text,imagePath text,birthdate text)");
        }, function (error) { alert(error.message); };
        

         self.change = function (data) {
            
             return dBa.query('UPDATE personalinfo SET name ="' + data.name + '",surname="' + data.surname + '",idNumber="' + data.idnumber + '",emailAddress="' + data.emailaddress +
             '",language="' + data.language + '",gender="' + data.gender + '",idType="' + data.idtype + '",homePhoneNumber="' + data.homephone + '",workPhoneNumber="' + data.workphone +
             '",medicalAidName="' + data.medaidname + '",medicalAidNumber="' + data.medaidnumber + '",homeAddress="' + data.homeaddress + '",medicalAidScheme="' + data.medaidscheme +
             '",workAddress="' + data.workaddress + '",emergencyContactName1="' + data.contactname1 + '",emergencyContactNumber1="' + data.contactnumber1 +
             '",emergencyContactName2="' + data.contactname2 + '",emergencyContactNumber2="' + data.contactnumber2 + '",occupation="' + data.occupation + '",businessName="' + data.businessname +
             '",bloodType="' + data.bloodtype + '",bloodDonorNumber="' + data.donornumber + '",allergies="' + data.allergies + '",homeLatitude="' + data.homelatitude +
             '",homeLongitude="' + data.homelongitude + '",imagePath="' + data.imagepath + '",birthdate="' + data.birthdate + '"  WHERE id = ' + data.id);

             //,idNumber = ?,emailAddress = ?,language = ?,gender = ?,idType = ?,homePhoneNumber = ?,workPhoneNumber = ?,medicalAidName = ?" +
             //",medicalAidNumber = ?,homeAddress = ?,medicalAidScheme = ?,workAddress = ?,emergencyContactName1 = ?,emergencyContactNumber1 = ?,emergencyContactName2 = ?,emergencyContactNumber2 = ?"+
             //",occupation = ?,businessName = ?,bloodType = ?,bloodDonorNumber = ?,allergies,homeLatitude = ?,homeLongitude = ?,imagePath = ?,birthdate =?

            //, data.idnumber, data.emailaddress, data.language, data.gender, data.idtype, data.homephone, data.workphone, data.medaidname
            //, data.medaidnumber, data.homeaddress, data.medaidscheme, data.workaddress, data.contactname1, data.contactnumber1, data.contactname2, data.contactnumber2
            //, data.occupation, data.businessname, data.bloodtype, data.donornumber, data.allergies, data.homelatitude, data.homelongitude, data.imagepath, data.birthdate

        }

        return self;
    })
    // Dependant Info Data
    .factory("dependantInfo", function (dBa) {
        var self = this;
        self.all = function () {
            return dBa.query("SELECT serverID,name,surname,idNumber,emailAddress,language,gender,idType,homePhoneNumber,workPhoneNumber,medicalAidName,medicalAidNumber,homeAddress," +
                    "medicalAidScheme,workAddress,emergencyContactName1,emergencyContactNumber1,emergencyContactName2,emergencyContactNumber2,occupation,businessName," +
                    "bloodType,bloodDonorNumber,allergies,homeLatitude,homeLongitude,imagePath FROM dependantinfo")
                .then(function (result) {
                    return dBa.getAll(result);
                }, function (error) { alert(error.message); });
        }, function (error) { alert(error.message); };

        self.add = function (dependantinfodata) {
            var parameters = [
                dependantinfodata.serverID, dependantinfodata.name, dependantinfodata.surname, dependantinfodata.idNumber, dependantinfodata.emailAddress, dependantinfodata.language, dependantinfodata.gender, dependantinfodata.idType, dependantinfodata.homePhoneNumber,
                dependantinfodata.serverID, dependantinfodata.name, dependantinfodata.surname, dependantinfodata.idNumber, dependantinfodata.emailAddress, dependantinfodata.language, dependantinfodata.gender, dependantinfodata.idType, dependantinfodata.homePhoneNumber,
                dependantinfodata.workPhoneNumber, dependantinfodata.medicalAidName, dependantinfodata.medicalAidNumber, dependantinfodata.homeAddress, dependantinfodata.medicalAidScheme, dependantinfodata.workAddress,
                dependantinfodata.emergencyContactName1, dependantinfodata.emergencyContactNumber1, dependantinfodata.emergencyContactName2, dependantinfodata.emergencyContactNumber2,
                dependantinfodata.occupation, dependantinfodata.businessName, dependantinfodata.bloodType, dependantinfodata.bloodDonorNumber, dependantinfodata.allergies, dependantinfodata.homeLatitude, dependantinfodata.homeLongitude, dependantinfodata.imagePath
            ];
            return dBa.query("INSERT INTO personalinfo (serverID,name,surname,idNumber,emailAddress,language,gender,idType,homePhoneNumber,workPhoneNumber,medicalAidName,medicalAidNumber,homeAddress," +
                "medicalAidScheme,workAddress,emergencyContactName1,emergencyContactNumber1,emergencyContactName2,emergencyContactNumber2,occupation,businessName," +
                "bloodType,bloodDonorNumber,allergies,homeLatitude,homeLongitude,imagePath) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)", parameters);
        }, function (error) { alert(error.message); };

        self.remove = function (dependantinfo) {
            var parameters = [dependantinfo.serverID];
            return dBa.query("DELETE FROM dependantinfo WHERE serverID = (?)", parameters);
        }, function (error) { alert(error.message); };

        self.count = function () {
            return dBa.query("SELECT Count(id) AS count FROM dependantinfo");
        }, function (error) { alert(error.message); };

        self.addData = function (dataHttpDependantInfoIn) {
            self.count().then(function (result) {
                if (result.rows.item(0).count < 1) {
                    self.add(dataHttpDependantInfoIn).then(function () { });
                    console.log('Adding Dependant Data ');
                }
            }, function (error) { alert(error.message); });
        }, function (error) { alert(error.message); };
        return self;
    })

    // Camera service 

    //quality 	            Number 	    The quality of the image, range 0-100
    //destinationType 	    Number 	    Format of the image.
    //sourceType 	        Number 	    Used for setting the source of the picture.
    //allowEdit 	        boolean 	Used for allowing editing of the image.
    //encodingType 	        Number 	    Value 0 will set JPEG, and value 1 will set PNG.
    //targetWidth 	        Number 	    Used for scaling image width.
    //targetHeight 	        Number 	    Used for scaling image height.
    //mediaType 	        string 	    Used for setting the media type.
    //cameraDirection 	    Number 	    Value 0 will set the back camera, and value 1 will set the front camera.
    //popoverOptions 	    string 	    iOS-only options that specify popover location in iPad.
    //saveToPhotoAlbum 	    boolean 	Used for saving image to photo album.
    //correctOrientation 	boolean 	Used for correcting orientation of the captured images.
    .factory('camera', function ($q) {

        return {
            getPicture: function (options) {
                var q = $q.defer();

                navigator.camera.getPicture(function (result) {
                    q.resolve(result);
                }, function (err) {
                    q.reject(err);
                }, options);

                return q.promise;
            }
        }

    })

    


    //Location
    .factory('location', function ($q) {
        return {
            getLocation: function (options) {
                var q = $q.defer();
                navigator.geolocation.getCurrentPosition(function (position) {
                    q.resolve(position);
                }, function (err) {
                    q.reject(err);
                });
                return q.promise;
            }
        };
    }, function (error) { alert(error.message); })

    //Setting
    .factory('settings', function (clientSelected, client) {
        var clientNum = null;
        return {
            getSelectedClient: function () {
                clientSelected.count().then(function (results) {
                    if (results.rows.item(0).count < 1) {
                        clientNum = "1";
                    } else {
                        clientSelected.all().then(function (dataIn) {
                            var clientName = dataIn[0].clientName;
                            client.single(clientName).then(function (result) {
                                clientNum = result.clientID;
                            }, function (error) { alert("Using Client Single: " + error.message); });
                        }, function (error) { alert("Using Client Single: " + error.message); });
                    }
                }, function (error) { alert(error.message); });

                return clientNum;
            }
        }
    }, function (error) { alert(error.message); })

    // Test if location services are on
    .factory('testGps', function ($ionicPopup) {
        return {
            test: function () {
                if (window.cordova) {
                    cordova.plugins.diagnostic.isLocationEnabled(function (enabled) {

                        var gpsEnabled;

                        (enabled ? gpsEnabled = true : gpsEnabled = false);

                        if (gpsEnabled === false) {
                            $ionicPopup.confirm({
                                title: "Gps Disconnected",
                                content: "Sorry, no Gps detected. Please enable location services."
                            })
                                .then(function (result) {
                                    if (result) {
                                        var isAndroid = ionic.Platform.isAndroid();
                                        //Test if Android
                                        if (isAndroid) {
                                            cordova.plugins.diagnostic.switchToLocationSettings();
                                        } else {
                                            $ionicPopup.confirm({
                                                title: "No location services detected",
                                                content: "Sorry, no Gps/Location services detected. Please switch Gps on."
                                            });
                                        }
                                    }
                                });
                        }
                    }, function (error) {
                        alert("The following error occurred: " + error.message);
                    });
                }
            }
        }
    })
    //Test if Network services are on Wifi etc
    .factory('testNetwork', function ($ionicPopup) {
        return {
            test: function () {

                // Check for network connection
                if (window.Connection) {
                    if (navigator.connection.type === Connection.NONE) {
                        $ionicPopup.confirm({
                            title: "Internet Disconnected",
                            content: "Sorry, no Internet connectivity detected, please reconnect and try again."
                        })
                        .then(function (result) {
                            if (!result) {
                                //ionic.Platform.exitApp();
                                if (navigator.app) {
                                    navigator.app.exitApp();
                                } else if (navigator.device) {
                                    navigator.device.exitApp();
                                }
                            } else {

                                var isAndroid = ionic.Platform.isAndroid();
                                //Test if Android
                                if (isAndroid) {
                                    cordova.plugins.diagnostic.switchToWifiSettings();
                                } else {
                                    $ionicPopup.confirm({
                                        title: "Internet Disconnected",
                                        content: "Sorry, no Internet connectivity detected. Please switch Wifi on."
                                    });
                                }
                            }
                        });
                    }
                }
            }
        }
    });



