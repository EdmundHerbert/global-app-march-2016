// Evil global variable
var db = null; // database reference

angular.module('globalApp', ['ionic', 'globalApp.controllers','ui.router', 'globalApp.services', 'ngCordova', 'ngMessages'])
    .run(function ($ionicPlatform, $cordovaSQLite, testNetwork, testGps, $ionicPopup) {

        $ionicPlatform.ready(function () {

            // Disable BACK button on home
            $ionicPlatform.registerBackButtonAction(function () {
                if (true) { // your check here
                    $ionicPopup.confirm({
                        title: 'System warning',
                        template: 'are you sure you want to exit?'
                    }).then(function (res) {
                        if (res) {
                            ionic.Platform.exitApp();
                        }
                    });
                }
            }, 100);

            // Test if connected to Network
            testNetwork.test();

            // Test if Location services are enabled
            testGps.test();


            // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
            // for form inputs)
            if (window.cordova && window.cordova.plugins && window.cordova.plugins.Keyboard) {
                cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
            }
            if (window.StatusBar) {
                // org.apache.cordova.statusbar required
                StatusBar.styleLightContent();
            }

            //Create databases
            db = $cordovaSQLite.openDB('globalapp.db');


            $cordovaSQLite.execute(db, "CREATE TABLE IF NOT EXISTS personalinfo (id integer primary key,serverID text,name text, surname text,idNumber text,emailAddress text,language text,gender text,idType text,homePhoneNumber text,workPhoneNumber text,medicalAidName text" +
                ",medicalAidNumber text,homeAddress text,medicalAidScheme text,workAddress text,emergencyContactName1 text,emergencyContactNumber1 text,emergencyContactName2 text,emergencyContactNumber2 text,occupation text,businessName text,bloodType text" +
                ",bloodDonorNumber text,allergies text,homeLatitude text,homeLongitude text,imagePath text,birthdate text)");

            $cordovaSQLite.execute(db, "CREATE TABLE IF NOT EXISTS dependantinfo (id integer primary key,serverID text,memberID text,relationship text,name text,surname text,idNumber text,emailAddress text,language text,gender text,idType text,dateOfBirth text,cellPhoneNumber text" +
                ",homePhoneNumber text,workPhoneNumber text,emergencyContactName1 text,emergencyContactNumber1 text,emergencyContactName2 text,emergencyContactNumber2 text,occupation text,businessName text, bloodType text,bloodDonorNumber text,allergies text," +
                "homeLatitude text,homeLongitude text,imagePath text)");

            $cordovaSQLite.execute(db, "CREATE TABLE IF NOT EXISTS driverslicence (id integer primary key,serverID text,memberID text,dependantID text,idNumber text,surname text,initials text,gender text,driverRestriction1 text,driverRestriction2 text,dateOfBirth text," +
                "certificateNumber text,countryOfIssue text,issueNumber text,validFrom text,validTo text,pdpCategory text,class1Code text,class1Restrictions text,class1IssueDate text,class2Code yexy,class2Restrictions text,class2IssueDate text,class2Code text," +
                "class3Restrictions text,class3IssueDate text,licencePhoto text)");

            $cordovaSQLite.execute(db, "CREATE TABLE IF NOT EXISTS vehiclelicence (id integer primary key,memberID text,dependantID text,controlNumber text,licenceNumber text,registerNumber text,vehicleDescription text,vehicleMake text,seriesName text,vehicleColour text," +
                "vinNumber text,engineNumber text,expiryDate text,vehicleYear text)");

            $cordovaSQLite.execute(db, "DROP TABLE PRODUCTS");

            $cordovaSQLite.execute(db, "CREATE TABLE IF NOT EXISTS products (id integer primary key,productID text,productName text,productIcon text,productClientId text)");

            $cordovaSQLite.execute(db, "CREATE TABLE IF NOT EXISTS accidentclaims (id integer primary key,datePointer text,incidentDate text,incidentTime text,incidentAddress text,incidentDescription text,incidentYourIDNumber text,incidentYourInitials text,incidentYourSurname text,incidentYourContactNumber text," +
                "vehiclePointer text,incidenttYourControlNumber text,incidentYourLicenceNumber text,incidentYourRegisterNumber text, incidentYourVehicleDescription text,incidentYourVehicleMake text,incidentYourSeriesName text,incidentYourVehicleColour text,incidentYourVinNumber text," +
                "incidentYourEngineNumber text,incidentYourExpiryDate text,incidentYourCompanyName text,incidentYourTowingCompanyName text,incidentYourTowingDriverName text,incidentYourTowingContact text,incidentPicFront text,incidentPicLeft text,incidentPicBack text,incidentPicRight text," +
                "incidentPicWindscreen text,incidentPicLabel text,incidentPicOdo text,incidentPicLicence text,submitted interger)");

            $cordovaSQLite.execute(db, "CREATE TABLE IF NOT EXISTS accidentthirdparty (id integer primary key,claimID text,thirdPartyNumber text,thirdPartyName text,thirdPartContact text,thirdPartyAddress text,thirdPartyControlNumber text,thirdPartLicenceNumber text,thirdPartRegisterNumber text,thirdPartyVehicleDescription text,thirdPartyVehicleMake text," +
                "thirdPartyVehicleSeries text,thirdPartyVehicleColour text,thirdPartyVinNumber text,thirdPartyEngineNumber text,thirdPartyExpiryDate text,thirdPartyPicFront text,thirdPartyPicLeft text,thirdPartyPicBack text,thirdPartyPicRight text,thirdPartPicWindscreen text,thirdPartyPicLabel text,thirdPartyPicOdo text," +
                "thirdPartyPicLicence text)");

            $cordovaSQLite.execute(db, "CREATE TABLE IF NOT EXISTS accidenteyewitness (id integer primary key,claimID text,eyewitnessNumber text,eyewitnessName text,eyewitnessContact text,eyewitnessAddress text)");

            $cordovaSQLite.execute(db, "CREATE TABLE IF NOT EXISTS test (id integer primary key,firstName text,lastName text)");

            $cordovaSQLite.execute(db, "CREATE TABLE IF NOT EXISTS member (id integer primary key,memberID text,memberToken text)");

            $cordovaSQLite.execute(db, "CREATE TABLE IF NOT EXISTS client (id integer primary key,clientID text,clientCode text,clientName text)");

            $cordovaSQLite.execute(db, "CREATE TABLE IF NOT EXISTS clientSelected (id integer primary key,selected text,clientName text)");

            $cordovaSQLite.execute(db, "CREATE TABLE IF NOT EXISTS termsConditions (id integer primary key,dateAccepted text)");

        });

    }).config(function ($stateProvider, $urlRouterProvider, $ionicConfigProvider) {

        $ionicConfigProvider.views.maxCache(0);

        $stateProvider

            // views before login without sidemenu
            .state('walkthrough', {
                cache: false,
                url: '/walkthrough',
                templateUrl: 'templates/walkthrough.html',
                controller: 'WalkthroughCtrl'
            })
            .state('login', {
                cache: false,
                url: '/login',
                templateUrl: 'templates/login.html',
                controller: 'LoginCtrl'
            })
            .state('activate', {
                cache: false,
                url: '/activate',
                templateUrl: 'templates/activate.html',
                controller: 'ActivateCtrl'
            })

            // views after login with sidemenu
            .state('app', {
                cache: false,
                url: '/app',
                abstract: true,
                templateUrl: 'templates/menu.html',
                controller: 'AppCtrl'
            })
            .state('app.home', {
                cache: false,
                url: '/home',
                views: {
                    'menuContent': {
                        templateUrl: 'templates/home.html',
                        controller: 'HomeCtrl'
                    }
                }
            })
            .state('app.forgot', {
                url: '/forgot',
                views: {
                    'menuContent': {
                        templateUrl: 'templates/forgot.html',
                        controller: 'ForgotCtrl'
                    }
                }
            })
            .state('app.emergency-assist', {
                cache: false,
                url: '/emergency-assist',
                views: {
                    'menuContent': {
                        templateUrl: 'templates/emergency-assist.html',
                        controller: 'EmergencyAssistCtrl'
                    }
                }
            })
            .state('app.emergency-assist-information', {
                url: '/emergency-assist-information',
                views: {
                    'menuContent': {
                        templateUrl: 'templates/emergency-assist-information.html',
                        controller: 'EmergencyAssistInfoCtrl'
                    }
                }
            })
            .state('app.emergency-assist-terms', {
                url: '/emergency-assist-terms',
                views: {
                    'menuContent': {
                        templateUrl: 'templates/emergency-assist-terms.html',
                        controller: 'EmergencyAssistTermsCtrl'
                    }
                }
            })
            .state('app.accident-assist', {
                cache: false,
                url: '/accident-assist',
                views: {
                    'menuContent': {
                        templateUrl: 'templates/accident-assist.html',
                        controller: 'AccidentAssistCtrl'
                    }
                }
            })
            .state('app.accident-step1', {
                url: '/accident-step1',
                views: {
                    'menuContent': {
                        templateUrl: 'templates/accident-step1.html',
                        controller: 'AccidentStep1Ctrl'
                    }
                }
            })
            .state('app.accident-step2', {
                url: '/accident-step2',
                views: {
                    'menuContent': {
                        templateUrl: 'templates/accident-step2.html',
                        controller: 'AccidentStep2Ctrl'
                    }
                }
            })
            .state('app.accident-step3', {
                url: '/accident-step3',
                views: {
                    'menuContent': {
                        templateUrl: 'templates/accident-step3.html',
                        controller: 'AccidentStep3Ctrl'
                    }
                }
            })
            .state('app.accident-step4', {
                url: '/accident-step4',
                views: {
                    'menuContent': {
                        templateUrl: 'templates/accident-step4.html',
                        controller: 'AccidentStep4Ctrl'
                    }
                }
            })
            .state('app.accident-step5', {
                url: '/accident-step5',
                views: {
                    'menuContent': {
                        templateUrl: 'templates/accident-step5.html',
                        controller: 'AccidentStep5Ctrl'
                    }
                }
            })
            .state('app.accident-step6', {
                url: '/accident-step6',
                views: {
                    'menuContent': {
                        templateUrl: 'templates/accident-step6.html',
                        controller: 'AccidentStep6Ctrl'
                    }
                }
            })
            .state('app.accident-list', {
                url: '/accident-list',
                views: {
                    'menuContent': {
                        templateUrl: 'templates/accidentlist.html',
                        controller: 'AccidentlistCtrl'
                    }
                }
            })
            .state('app.personal', {
                url: '/personal',
                
                views: {
                    'menuContent': {
                        templateUrl: 'templates/personal.html',
                        controller: 'PersonalCtrl'
                    }
                }
            })
            .state('app.personal-menu', {
                url: '/personal-menu',
                views: {
                    'menuContent': {
                        templateUrl: 'templates/personal-menu.html',
                        controller: 'PersonalMenuCtrl'
                    }
                }
            })
            .state('app.personal-detail', {
                url: '/personal-detail',
                views: {
                    'menuContent': {
                        templateUrl: 'templates/personal-details.html',
                        controller: 'PersonalDetailCtrl'
                    }
                }
            })
            .state('app.drivers-license', {
                url: '/drivers-license',
                views: {
                    'menuContent': {
                        templateUrl: 'templates/drivers-license.html',
                        controller: 'DriversLicenseCtrl'
                    }
                }
            })
            .state('app.vehicle-licenses', {
                url: '/vehicle-licenses',
                views: {
                    'menuContent': {
                        templateUrl: 'templates/vehicle-licenses.html',
                        controller: 'VehicleLicensesCtrl'
                    }
                }
            })
            .state('app.vehicle-license-detail', {
                url: '/vehicle-license-detail',
                views: {
                    'menuContent': {
                        templateUrl: 'templates/vehicle-license-detail.html',
                        controller: 'VehicleLicenseDetailCtrl'
                    }
                }
            })
            .state('app.profile-picture', {
                url: '/profile-picture',
                views: {
                    'menuContent': {
                        templateUrl: 'templates/profile-picture.html',
                        controller: 'ProfilePictureCtrl'
                    }
                }
            })
            .state('app.chat-messages', {
                url: '/chat-messages',
                views: {
                    'menuContent': {
                        templateUrl: 'templates/chat-messages.html',
                        controller: 'ChatMessagesCtrl'
                    }
                }
            })
            .state('app.chat', {
                url: '/chat',
                views: {
                    'menuContent': {
                        templateUrl: 'templates/chat.html',
                        controller: 'ChatCtrl'
                    }
                }
            })
            .state('app.chat-history', {
                url: '/chat-history',
                views: {
                    'menuContent': {
                        templateUrl: 'templates/chat-history.html',
                        controller: 'ChatHistoryCtrl'
                    }
                }
            })
            .state('app.chat-history-detail', {
                url: '/chat-history-detail',
                views: {
                    'menuContent': {
                        templateUrl: 'templates/chat-history-detail.html',
                        controller: 'ChatHistoryDetailCtrl'
                    }
                }
            })
            .state('app.messages-unread', {
                url: '/messages-unread',
                views: {
                    'menuContent': {
                        templateUrl: 'templates/messages-unread.html',
                        controller: 'MessagesUnreadCtrl'
                    }
                }
            })
            .state('app.messages-read', {
                url: '/messages-read',
                views: {
                    'menuContent': {
                        templateUrl: 'templates/messages-read.html',
                        controller: 'MessagesReadCtrl'
                    }
                }
            })
            .state('app.about-menu', {
                url: '/about-menu',
                views: {
                    'menuContent': {
                        templateUrl: 'templates/about-menu.html',
                        controller: 'AboutMenuCtrl'
                    }
                }
            })
            .state('app.about', {
                url: '/about',
                views: {
                    'menuContent': {
                        templateUrl: 'templates/about.html',
                        controller: 'AboutCtrl'
                    }
                }
            })
            .state('app.contact', {
                url: '/contact',
                views: {
                    'menuContent': {
                        templateUrl: 'templates/contact.html',
                        controller: 'ContactCtrl'
                    }
                }
            })
            .state('app.branches', {
                url: '/branches',
                views: {
                    'menuContent': {
                        templateUrl: 'templates/branches.html',
                        controller: 'BranchesCtrl'
                    }
                }
            })
            .state('app.services', {
                url: '/services',
                views: {
                    'menuContent': {
                        templateUrl: 'templates/services.html',
                        controller: 'ServicesCtrl'
                    }
                }
            })
            .state('app.partners', {
                url: '/partners',
                views: {
                    'menuContent': {
                        templateUrl: 'templates/partners.html',
                        controller: 'PartnersCtrl'
                    }
                }
            })
            .state('app.partner-details', {
                url: '/partner-details',
                views: {
                    'menuContent': {
                        templateUrl: 'templates/partner-details.html',
                        controller: 'PartnerDetailsCtrl'
                    }
                }
            })
            .state('app.help', {
                url: '/help',
                views: {
                    'menuContent': {
                        templateUrl: 'templates/help.html',
                        controller: 'HelpCtrl'
                    }
                }
            })
            .state('app.settings', {
                cache: false,
                url: '/settings',
                views: {
                    'menuContent': {
                        templateUrl: 'templates/settings.html',
                        controller: 'SettingsCtrl'
                    }
                }
            })
            .state('app.terms', {
                cache: false,
                url: '/terms',
                views: {
                    'menuContent': {
                        templateUrl: 'templates/termsandcon.html',
                        controller: 'TermsCtrl'
                    }
                }
            });
        //.state('app.terms', {
        //    url: '/terms',
        //    views: {
        //        'menuContent': {
        //            templateUrl: 'templates/terms.html',
        //            controller: 'TermsCtrl'
        //        }
        //    }
        //});
        // if none of the above states are matched, use this as the fallback
        $urlRouterProvider.otherwise('/walkthrough');
    });


