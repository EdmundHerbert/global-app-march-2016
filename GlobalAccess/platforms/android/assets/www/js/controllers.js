angular.module('globalApp.controllers', ['ngStorage'])
    .controller('AppCtrl', function () { })
    .controller('WalkthroughCtrl', function ($scope, $state, $ionicSlideBoxDelegate) {

        $scope.navSlide = function (index) {
            $ionicSlideBoxDelegate.slide(index, 500);
        }
        $scope.Login = function () {
            $state.go('login');
        };
        $scope.Activate = function () {
            $state.go('activate');
        };
    })
    .controller('LoginCtrl', function ($scope, settings, $window, $ionicHistory, termsConditions, clientSelected, $localStorage, $ionicPopup, $state, httpServiceSecond, client, personalInfo, vehicleLicence) {

        // Clear cache so events can fire everytime function called.
        $scope.$on("$ionicView.afterLeave", function () {
            $ionicHistory.clearCache();
        });
        $localStorage.clientId = "";
        $localStorage.firstTime = false;
        var dataHttpIn = null;
        // MobileMemberLogin function 
        var methodLogin = "MobileMemberLogin";
        var dataHttpPersonalInfoIn = null;
        // MobileGetPersonalInfo function
        var method = 'MobileGetPersonalInfo';
        var clientName = null;


        $localStorage.clientId = settings.getSelectedClient();

        // On First Time startup no clientId because database new.
        if ($localStorage.clientId < 1 || $localStorage.clientId == null) {
            $localStorage.clientId = '1';
        }

        $scope.LogIn = function (user) {

            $scope.submitted = true;

            var dataIn = {
                phone: user.cell,
                pwd: user.pass,
                clientID: $localStorage.clientId
            };

            // Call to httpService (mobileAJAXServer) to get member data and client/product data
            httpServiceSecond.getUser(dataIn, methodLogin).then(function (response) {
                dataHttpIn = response;

                if (dataHttpIn.result === 'OK') { // add memberID and token to database
                    $localStorage.memberCellPhone = user.cell; // Users login cell phone number
                    $localStorage.dependantId = dataHttpIn.dependantID; //Dependants Id
                    $localStorage.memberId = dataHttpIn.memberID; // Member Id
                    $localStorage.membertoken = dataHttpIn.memberToken; // Member Token
                    $localStorage.clientName = dataHttpIn.clientName; // Client Name
                    $localStorage.bannerImage = []; // Array of banner images for client
                    $localStorage.allBanners = []; //Array of Banners for each client
                    $localStorage.clientOrder = []; // Array of ClientIds and index's

                    // Save client banners to local storage
                    for (var i = 0; i < dataHttpIn.clients.length; i++) {
                        $localStorage.clientOrder.push({ index: i, clientId: dataHttpIn.clients[i].clientID });
                        $localStorage.bannerImage = dataHttpIn.clients[i].banners;
                        $localStorage.allBanners.push($localStorage.bannerImage);
                    }

                    // Set cell number for panic
                    $localStorage.phone = user.cell;

                    // Input locally stored memberID
                    var datamemberIdNo = {
                        memberID: $localStorage.memberId,
                        dependantID: $localStorage.dependantId
                    };

                    // Get PersonInfo data from server
                    httpServiceSecond.getUser(datamemberIdNo, method).then(function (data) {
                        dataHttpPersonalInfoIn = data;
                        // Test if PersonalInfo database is empty, if not add data
                        personalInfo.drop().then(function () {
                            personalInfo.create().then(function () {
                                personalInfo.add(dataHttpPersonalInfoIn).then(function () {
                                    function wait() {
                                        client.testcount(dataHttpIn);
                                        vehicleLicence.getvehicledata(); //.then(function () {
                                        clientSelected.count().then(function (result) {
                                            if (result.rows.item(0).count > 0) {
                                                clientSelected.all().then(function (dataIn) {
                                                    clientName = dataIn[0].clientName;
                                                    client.single(clientName).then(function (result) {
                                                        $localStorage.clientId = result;
                                                    }, function (error) { alert(error.message); });
                                                }, function (error) { alert(error.message); });
                                            }
                                        }, function (error) { alert(error.message); });
                                    }

                                    setTimeout(wait, 2500); // was 2000
                                }, function (error) { alert(error.message); });
                            }, function (error) { alert(error.message); });
                        }, function (error) { alert(error.message); });

                        function normalise() {
                            termsConditions.count().then(function (results) {
                                if (results.rows.item(0).count > 0) {
                                    $state.go('app.home');
                                } else {
                                    $state.go('app.terms');
                                }
                            });
                        }

                        setTimeout(normalise(), 3000); // was 2800
                    }, function (error) { alert(error.message); });

                } else {
                    var alertPopup = $ionicPopup.alert({
                        title: 'Login problem!',
                        template: 'Login failed, please re-enter your login details.',
                        buttons: [
                            {
                                text: 'Ok',
                                type: 'button-colour-secondary',
                                onTap: function () {
                                    user.cell = "";
                                    user.pass = "";
                                }
                            }
                        ]
                    });
                };
            }, function (error) { alert(error.message); });
        }
        $scope.Forgot = function () { $state.go('app.forgot'); };
    })


    //Chat stuff as on 20160222 tested working!!!!!
    //alert('here1');

    //$scope.ws = new WebSocket("ws://globalapp.co.za:443/HaloCallCentre/mobilechatendpoint/M4");
    //$scope.ws.onopen = function () {

    //    alert('Connected');
    //    $timeout(sendData, 100);
    //};
    //$scope.ws.onerror = function (event) {
    //    var myError = event.data;
    //    alert(myError);
    //};
    //function sendData() {
    //    alert('Send Data');
    //    var request = '{"message":"","command":"memberChatRequest","sentFromID":"24","sentFromName":"0832908052","sentTo": "0" }';
    //    $scope.ws.send(request);
    //}
    //$scope.ws.onmessage = function (event) {
    //    var foo = event.data;
    //    alert(foo);
    //};

    // Test function in array
    //.controller('TestCtrl',[function() {
    //    this.hellomessage = 'Hello';
    //    var good = 'GoodBye';
    //}])
    .controller('ForgotCtrl', function ($scope, $state, $ionicPopup, $ionicModal, httpServiceSecond, $state) {

        $scope.resetPassword = function (user) {

            var dataInResetPassword = { phone: user.cell };

            // MobileMemberResetPassword function 
            var methodResetPassword = "MobileMemberResetPassword";

            // Reset password request sent to server
            httpServiceSecond.getUser(dataInResetPassword, methodResetPassword).then(function (data) {
                console.log(data);
            });
            var alertPopup = $ionicPopup.alert({
                title: 'Request successful!',
                template: 'Your password has been reset, you will shortly receive a sms containing you activation code.',
                buttons: [
                    {
                        text: 'Ok',
                        type: 'button-colour-secondary',
                        onTap: function (e) {
                            $state.go('app.home');
                        }
                    }
                ]
            });
            alertPopup.then(function () {
                console.log('Reset Successful');
                $state.go('walkthrough');
            });
        };
    })
    .controller('prodCtrl', function ($scope, product, $state, $ionicModal, $ionicHistory, $localStorage) {

        // Clear cache so events can fire everytime function called.
        $scope.$on("$ionicView.afterLeave", function () {
            $ionicHistory.clearCache();
        });

        //If clientId = empty or null then default to Global app 
        if ($localStorage.clientId < 1 || $localStorage.clientId === null) {
            $localStorage.clientId = "1";
        }

        $scope.counter = 0;

        $scope.openCountdown = function () {
            $ionicModal.fromTemplateUrl('templates/panic-countdown.html', {
                scope: $scope
            }).then(function (modal) {
                $scope.modal = modal;
                $scope.modal.show();
            });
        };

        $scope.closeCountdown = function () {
            $scope.modal.hide();
            $state.go('app.home');
        };

        $scope.counterplus = function () {
            $scope.counter = $scope.counter + 1;
        };

        $scope.selectedIdUpdate = function (data) {
            product.productName(data.name).then(function (result) {
                $localStorage.selectedProductID = result.productID;
            });

            $scope.openCountdown();
            $scope.modal.hide();
        }

        $scope.productData = [];

        product.many($localStorage.clientId).then(function (resultData) {
            for (var y = 0; y < resultData.length; y++) {
                var newElementTwo = {}
                newElementTwo['name'] = resultData[y].productName;
                newElementTwo['icon'] = resultData[y].productIcon;
                $scope.productData.push(newElementTwo);
            }
        });
    })
    .controller('ActivateCtrl', function ($scope, httpServiceSecond, $state) {

        $scope.activate = function (user) {
            var resetPasswordActivate = {
                phone: user.cell,
                pwd: user.password,
                code: user.activation
            };

            var methodSendActivation = "MobileActivateAccount";

            // Reset password sent to server
            httpServiceSecond.getUser(resetPasswordActivate, methodSendActivation).then(function (data) {
                console.log(data);
                $state.go('login');
            });
        };
    })
    .controller('PersonalCtrl', function ($scope, $localStorage, httpServiceSecond, dependantInfo, personalInfo) {

        if (!angular.isDefined($scope.picture)) {
            $scope.picture = "img/ph.png";
        }

        personalInfo.all().then(function (response) {
            if (response[0].imagePath !== "/images/members/default/anon.png") {
                $scope.picture = response[0].imagePath;
            } else {
                $scope.picture = "img/ph.png";
            }
        });

        var methodGetPersonalInfo = "MobileGetPersonalInfo";

        $scope.thisphone = $localStorage.memberCellPhone;

        var data = {
            memberID: $localStorage.memberId,
            dependantID: $localStorage.dependantId
        };

        if ($localStorage.dependantId > 0) {
            // Dependant use dependant function
            dependant(data);
            $scope.isDependant = 1;
            if (!angular.isDefined($localStorage.dependantimageString || $localStorage.dependantimageString === "")) {
                $localStorage.dependantimageString = "img/ph.png";
                $scope.dependantData.imageString = "img/ph.png";
            }
            dependantInfo.all().then(function (result) {
                $localStorage.dependatInfo = result;
            });
        } else {

            // Member use member function
            member();
            $scope.isDependant = 0;
            if (!angular.isDefined($localStorage.memberimageString || $localStorage.memberimageString === "")) {
                $localStorage.memberimageString = "img/ph.png";
                $scope.memberData.imageString = "img/ph.png";

            }
        };

        function member() {
            personalInfo.all().then(function (response) {
                $localStorage.memberData = response[0]; // get data from local database
                $scope.memberData = response[0];
                $localStorage.memberImageString = $localStorage.memberData.imagePath;
            });

        }

        function dependant(data) {
            httpServiceSecond.getUser(data, methodGetPersonalInfo).then(function (response) {
                $localStorage.dependantData = response;
                $scope.dependantData = response;
                $localStorage.dependantImage = $localStorage.dependantData.imageString;
            });
        }
    })
    .controller('SettingsCtrl', function ($scope, settings, $ionicModal, $state, $ionicPopover, $localStorage, clientSelected, client) {
        $scope.set = angular.isDefined($localStorage.autologin);

        // populate clients if ClientSelected larger than 1
        $scope.clients = [];
        clientSelected.count().then(function (results) {
            if (results.rows.item(0).count > 0) {
                clientSelected.all().then(function (result) {
                    var clientName = result[0].clientName;
                    client.count().then(function (result) {
                        if (result.rows.item(0).count > 0) {
                            client.all().then(function (resultClient) {
                                for (var j = 0; j < resultClient.length; j++) {
                                    var newElementOne = {}
                                    if (resultClient[j].clientName === clientName) {
                                        newElementOne['name'] = resultClient[j].clientName;
                                        newElementOne['enabled'] = true;
                                    } else {
                                        newElementOne['name'] = resultClient[j].clientName;
                                        newElementOne['enabled'] = false;
                                    }
                                    $scope.clients.push(newElementOne);
                                }
                            });
                        }
                    });
                });
            }
        });
        // populate clients if ClientSelected = 0
        clientSelected.count().then(function (results) {
            if (results.rows.item(0).count < 1) {
                client.all().then(function (result) {
                    for (var s = 0; s < result.length; s++) {
                        var newElement = {}
                        if (s === 0) {
                            newElement['name'] = result[s].clientName;
                            newElement['enabled'] = true;
                        } else {
                            newElement['name'] = result[s].clientName;
                            newElement['enabled'] = false;
                        }
                        $scope.clients.push(newElement);
                    }
                });
            }
        });


        ($scope.set === false) ? $localStorage.autologin = true : $localStorage.autologin = false;

        $scope.tags = [
            {
                text: 'Auto login',
                enabled: $localStorage.autologin
            }
        ];

        var clientSelectedValue = "";

        $scope.clientNameSelected = function (data) {
            clientSelectedValue = data;
            clientSelected.count().then(function (results) {
                if (results.rows.item(0).count < 1) {
                    clientSelected.add(true, clientSelectedValue).then(function () { });
                    client.single(clientSelectedValue).then(function (result) {
                        $localStorage.clientId = result.clientID;
                    });
                } else {
                    clientSelected.change(clientSelectedValue, true).then(function () { });
                    client.single(clientSelectedValue).then(function (result) {
                        $localStorage.clientId = result.clientID;
                    });
                }
            });
        }

        $scope.updateAutoLogin = function (data) { $localStorage.autologin = data; }
    })
    .controller('MenuCtrl', function ($scope, $ionicModal, $ionicHistory) {

        $scope.$on("$ionicView.afterLeave", function () {
            $ionicHistory.clearCache();
        });
        $scope.closePanic = function () {
            $scope.modal.hide();
        };

        $scope.openPanic = function () {
            $ionicModal.fromTemplateUrl('templates/panic.html', {
                scope: $scope
            }).then(function (modal) {
                $scope.modal = modal;
                $scope.modal.show();
            });
        };
    })
    .controller('HomeCtrl', function ($scope, $state, $localStorage, $ionicModal, product, settings) {

        // Test if app loading for first time pull id from settings database
        if ($localStorage.firstTime === false) {
            $localStorage.clientId = settings.getSelectedClient();
            $localStorage.firstTime = true;
        }

        //If settings database empty(to be created)then set to default clientId = 1
        if ($localStorage.clientId < 1 || $localStorage.clientId == null) {
            $localStorage.clientId = "1";
        }

        // Populate products from selected client
        $scope.productData = [];
        product.many($localStorage.clientId).then(function (resultData) {
            for (var y = 0; y < resultData.length; y++) {
                var newElementTwo = {}
                newElementTwo['name'] = resultData[y].productName;
                newElementTwo['icon'] = resultData[y].productIcon;
                $scope.productData.push(newElementTwo);
            }
        });

        // On startup use default banners
        var flag = false;

        if ($localStorage.clientOrder.length > 1) {
            for (var i = 0; i < $localStorage.clientOrder.length; i++) {
                if ($localStorage.clientOrder[i].clientId === $localStorage.clientId) {
                    flag = true;
                    $scope.myImage = $localStorage.allBanners[$localStorage.clientOrder[i].index];
                }
            }
            if (!flag) {
                $scope.myImage = $localStorage.allBanners[0];
            }
        } else {
            $scope.myImage = $localStorage.allBanners[0];
        }

        $scope.closePanic = function () {
            $scope.modal.hide();
        };

        $scope.openPanic = function () {
            $ionicModal.fromTemplateUrl('templates/panic.html', {
                scope: $scope
            }).then(function (modal) {
                $scope.modal = modal;
                $scope.modal.show();
            });
        };
        $scope.$on('$destroy', function () {
            $scope.modal.remove();
        });
    })
    .controller('PanicCtrl', function ($scope, $ionicModal, $ionicHistory, $state) {
        // Clear cache
        $scope.$on("$ionicView.afterLeave", function () {
            $ionicHistory.clearCache();
        });

        $scope.loading = true;
        $scope.loading = false;
        $scope.openCountdown = function () {
            $ionicModal.fromTemplateUrl('templates/panic-countdown.html', {
                scope: $scope
            }).then(function (modal) {
                $scope.modal = modal;
                $scope.modal.show();
            });
        };
        $scope.closeCountdown = function () {
            $scope.modal.hide();
            $state.go('app.home');
        };
    })
    .controller('PanicCountdownCtrl', function ($scope, $ionicPopup) {
        $scope.showAlert = function () {
            var alertPopup = $ionicPopup.alert({
                title: 'Success!',
                template: 'Your request for 24 Hour Total Care has been sent successfully.',
                scope: $scope,
                buttons: [
                    {
                        text: 'Ok',
                        type: 'button-colour-secondary'
                    }
                ]
            });
            alertPopup.then(function (res) {
                console.log('24 Hour Care request successful ' + res);
            });
        };
    })
    .controller('EmergencyAssistCtrl', function ($scope, $ionicModal, $state) {

        $scope.openPanic = function () {
            $ionicModal.fromTemplateUrl('templates/panic.html', {
                scope: $scope
            }).then(function (modal) {
                $scope.modal = modal;
                $scope.modal.show();
            });
        };
        $scope.closePanic = function () {
            $scope.modal.hide();
            $state.go('app.home');
        };

        $scope.openProviders = function (animation) {
            $ionicModal.fromTemplateUrl('templates/service-providers.html', {
                scope: $scope,
                animation: animation
            }).then(function (modal) {
                $scope.modal = modal;
                $scope.modal.show();
            });
        };
        $scope.closeProviders = function () {
            $scope.modal.hide();
            $state.go('app.home');
        };

        $scope.$on('$destroy', function () {
            $scope.modal.remove();
        });
    })
    .controller('EmergencyAssistInfoCtrl', function ($scope) {
        $scope.loading = true;
        $scope.loading = false;
    })
    .controller('EmergencyAssistTermsCtrl', function ($scope) {
        //$scope.loading = true;
        //$scope.loading = false;
    })
    .controller('AccidentAssistCtrl', function ($scope) {
        //$scope.loading = true;
        //$scope.loading = false;
    })
    .controller('AccidentStep1Ctrl', function ($scope, $state) {
        //$scope.loading = true;
        //$scope.loading = false;
        $scope.Next = function () {
            $state.go('app.accident-step2');
        };
    })
    .controller('AccidentStep2Ctrl', function ($scope, $state) {
        //$scope.loading = true;
        //$scope.loading = false;
        $scope.Next = function () {
            $state.go('app.accident-step3');
        };
    })
    .controller('AccidentStep3Ctrl', function ($scope, $state) {
        //$scope.loading = true;
        //$scope.loading = false;
        $scope.Next = function () {
            $state.go('app.accident-step4');
        };
    })
    .controller('AccidentStep4Ctrl', function ($scope, $state) {
        //$scope.loading = true;
        //$scope.loading = false;
        $scope.Next = function () {
            $state.go('app.accident-step5');
        };
    })
    .controller('AccidentStep5Ctrl', function ($scope, $state) {
        //$scope.loading = true;
        //$scope.loading = false;
        $scope.Next = function () {
            $state.go('app.accident-step6');
        };
    })
    .controller('AccidentlistCtrl', function () {

    })
    .controller('AccidentStep6Ctrl', function ($scope, $state) {
        //$scope.loading = true;
        //$scope.loading = false;
    })

//.controller('PersonalCtrl', function ($scope) {
//    //$scope.loading = true;
//    //$scope.loading = false;
//})
    .controller('PersonalMenuCtrl', function ($scope, personalInfo, $localStorage) {

        if (!angular.isDefined($scope.picture)) {
            $scope.picture = "img/ph.png";
        }

        personalInfo.all().then(function (response) {
            $scope.thisphone = $localStorage.memberCellPhone;
            $localStorage.memberData = response[0]; // get data from local database
            $scope.memberData = response[0];

            if (response[0].imagePath !== "/images/members/default/anon.png") {
                $scope.picture = response[0].imagePath;
            } else {
                $scope.picture = "img/ph.png";
            }

        });
    })
    .controller('PersonalDetailCtrl', function ($scope, personalInfo, $localStorage, camera, $filter, httpServiceSecond, $ionicPopup, $state) {

        if (!angular.isDefined($scope.picture)) {
            $scope.picture = "img/ph.png";
        } else {
            $scope.picture = $localStorage.picture;
        }
        if (!angular.isDefined($scope.user)) {
            $scope.user = {
                birthday: null
            };
        };

        if ($scope.user.birthday === null) {
            $localStorage.birthday = new Date();
        }

        //example of using the camera
        $scope.takePicture = function (options) {

            var optionsSet = {
                quality: 80,
                destinationType: Camera.DestinationType.DATA_URL,
                sourceType: Camera.PictureSourceType.CAMERA,
                allowEdit: false,
                encodingType: Camera.EncodingType.JPEG,
                targetWidth: 100,
                targetHeight: 100,
                popoverOptions: CameraPopoverOptions,
                saveToPhotoAlbum: false,
                correctOrientation: true
            };

            camera.getPicture(optionsSet).then(function (imageData) {
                $scope.$apply($scope.picture = "data:image/jpeg;base64," + imageData);
            }, function (err) {
                console.log(err);
            });

            $scope.$apply($localStorage.picture = $scope.picture);


        };

        personalInfo.all().then(function (response) {

            $localStorage.memberData = response[0]; // get data from local database
            $scope.memberData = response[0];

            if (response[0].birthdate === null) {
                $localStorage.birthday = new Date();
            } else {
                $localStorage.birthday = response[0].birthdate;
            }

            $scope.user.birthday = new Date($localStorage.birthday);

            if (response[0].imagePath !== "/images/members/default/anon.png") {
                $scope.picture = response[0].imagePath;

            } else {
                $scope.picture = "img/ph.png";
            }

            $scope.genders = [{ name: "Male", id: 1 }, { name: "Female", id: 2 }];

            if ($scope.memberData.gender === "") {
                $scope.memberData.gender = $scope.genders[0];
            } else {
                for (var i = 0; i < $scope.genders.length; i++) {
                    if (response[0].gender === $scope.genders[i].name) {
                        $scope.memberData.gender = $scope.genders[i];
                    }
                }
            }

            $scope.bloodGroups = [{ name: "O+", id: 1 }, { name: "O-", id: 2 }, { name: "A+", id: 3 }, { name: "A-", id: 4 }, { name: "B+", id: 5 }, { name: "B-", id: 6 }, { name: "AB+", id: 7 }, { name: "AB-", id: 8 }];

            if ($scope.memberData.bloodType === "") {
                $scope.memberData.bloodType = $scope.bloodGroups[2];
            } else {
                for (var j = 0; j < $scope.bloodGroups.length; j++) {
                    if (response[0].bloodType === $scope.bloodGroups[j].name) {
                        $scope.memberData.bloodType = $scope.bloodGroups[j];
                    }
                }
            }

            $scope.idTypes = [{ name: "SA ID", id: 1 }, { name: "Passport", id: 2 }];

            if ($scope.memberData.idType === "") {
                $scope.memberData.idType = $scope.idTypes[2];
            } else {
                for (var p = 0; p < $scope.idTypes.length; p++) {
                    if (response[0].idType === $scope.idTypes[p].name) {
                        $scope.memberData.idType = $scope.idTypes[p];
                    }
                }
            }

            $scope.cellPhone = $localStorage.memberCellPhone;
        });

        var dataEdit = {};

        $scope.updatePersonal = function (data) {
            $scope.submitted = true;


            if ($localStorage.dependantId < 1) {
                dataEdit = {
                    id: data.id,
                    name: data.name,
                    surname: data.surname,
                    workphone: data.workPhoneNumber,
                    homephone: data.homePhoneNumber,
                    birthdate: $scope.user.birthday,
                    emailaddress: data.emailAddress,
                    occupation: data.occupation,
                    businessname: data.businessName,
                    idnumber: data.idNumber,
                    idtype: data.idType.name,
                    gender: data.gender.name,
                    language: data.language,
                    medaidname: data.medicalAidName,
                    medaidnumber: data.medicalAidNumber,
                    medaidscheme: data.medicalAidScheme,
                    bloodtype: data.bloodType.name,
                    donornumber: data.bloodDonorNumber,
                    allergies: data.allergies,
                    contactname1: data.emergencyContactName1,
                    contactnumber1: data.emergencyContactNumber1,
                    contactname2: data.emergencyContactName2,
                    contactnumber2: data.emergencyContactNumber2,
                    homeaddress: data.homeAddress,
                    workaddress: data.workAddress,
                    imagepath: $scope.picture,
                    homelatitude: $localStorage.latitude,
                    homelongitude: $localStorage.longitude
                };
            }

            var method = "MobileUpdateMemberDetails";

            var dataIn = {
                memberID: $localStorage.memberId,
                name: data.name,
                surname: data.surname,
                cellphone: $localStorage.memberCellPhone,
                workphone: data.workPhoneNumber,
                homephone: data.homePhoneNumber,
                dateofbirth: $scope.user.birthday,
                emailaddress: data.emailAddress,
                occupation: data.occupation,
                businessname: data.businessName,
                idnumber: "" + data.idNumber,
                idtype: data.idType.name,
                gender: data.gender.name,
                language: data.language,
                medaidname: data.medicalAidName,
                medaidnumber: data.medicalAidNumber,
                medaidscheme: data.medicalAidScheme,
                bloodtype: data.bloodType.name,
                donornumber: data.bloodDonorNumber,
                allergies: data.allergies,
                contactname1: data.emergencyContactName1,
                contactnumber1: data.emergencyContactNumber1,
                contactname2: data.emergencyContactName2,
                contactnumber2: data.emergencyContactNumber2,
                homeaddress: data.homeAddress,
                workaddress: data.workAddress
            }

            personalInfo.change(dataEdit).then(function () { });
            httpServiceSecond.getUser(dataIn, method).then(function () { });

            var methodImage = "MobileMemberImage";

            var dataInImage = {
                memberID: $localStorage.memberId,
                imageString: $scope.picture
            }

            httpServiceSecond.getUser(dataInImage, methodImage).then(function () { });// Save image to server


            var alertPopup = $ionicPopup.alert({
                title: 'Personal Info saved',
                template: 'You have saved your personal data.',
                buttons: [
                    {
                        text: 'Ok',
                        type: 'button-colour-secondary',
                        onTap: function () {
                            $state.go('app.home');
                        }
                    }
                ]
            });
        }
    })

.controller('DriversLicenseCtrl', function ($scope) {
    //$scope.loading = true;
    //$scope.loading = false;
})

.controller('VehicleLicensesCtrl', function ($scope) {
    //$scope.loading = true;
    //$scope.loading = false;
})

.controller('VehicleLicenseDetailCtrl', function ($scope) {
    //$scope.loading = true;
    //$scope.loading = false;
})

.controller('ProfilePictureCtrl', function ($scope) {
    //$scope.loading = true;
    //$scope.loading = false;
})

.controller('AboutMenuCtrl', function ($scope) {
    //$scope.loading = true;
    //$scope.loading = false;
})

.controller('AboutCtrl', function ($scope) {
    //$scope.loading = true;
    //$scope.loading = false;
})

.controller('ContactCtrl', function ($scope) {
    //$scope.loading = true;
    //$scope.loading = false;
})

.controller('BranchesCtrl', function ($scope) {
    //$scope.loading = true;
    //$scope.loading = false;
})

.controller('ServicesCtrl', function ($scope) {
    //$scope.loading = true;
    //$scope.loading = false;
})

.controller('PartnersCtrl', function ($scope) {
    //$scope.loading = true;
    //$scope.loading = false;
})

.controller('PartnerDetailsCtrl', function ($scope) {
    //$scope.loading = true;
    //$scope.loading = false;
})

.controller('HelpCtrl', function ($scope) {
    //$scope.loading = true;
    //$scope.loading = false;
})

.controller('TermsCtrl', function ($scope, $state, termsConditions, $filter) {
    //$scope.loading = true;
    //$scope.loading = false;

    $scope.acceptTerms = function () {
        // Terms and Conditions accepted save and date timestamp
        var termsdate = $filter('date')(new Date(), 'yyyy-MM-dd HH:mm:ss Z');
        termsConditions.add(termsdate).then(function () { });
        $state.go('app.home');

    }

    //User rejected terms and conditions exit application
    $scope.rejectTerms = function () {
        if (navigator.app) {
            navigator.app.exitApp();
        } else if (navigator.device) {
            navigator.device.exitApp();
        }
    }

}).controller('SendPanicCtrl', function ($scope, $ionicHistory, $ionicPlatform, $timeout, $localStorage, location, testNetwork, testGps, $state) {

    $scope.cancel = 0;
    var latitude = "";
    var longitude = "";

    $scope.counter = 5;
    $scope.onTimeout = function () {
        $scope.counter--;
        if ($scope.counter > 0) {
            mytimeout = $timeout($scope.onTimeout, 1000);
            if ($scope.cancel == 1) {
                $timeout.cancel(mytimeout);
                $scope.modal.hide();
            }
        }
        else {
            if ($scope.cancel === 1) {
                $timeout.cancel(mytimeout);
                $scope.modal.hide();
                $scope.counter = "Cancelling..";

            }
            else {
                $timeout.cancel(mytimeout);
                $scope.modal.hide();
                $scope.sendPanic();
                $state.go('app.home');
            }
        }
    }
    var mytimeout = $timeout($scope.onTimeout, 1000);

    $scope.cancel = function () {
        $scope.cancel = 1;
        $state.go('app.home');
    }

    $scope.$on('$destroy', function () {
        $scope.modal.remove();
    });

    $scope.sendPanic = function () {
        $scope.ws = new WebSocket("ws://globalapp.co.za:443/HaloCallCentre/mobilechatendpoint/M" + $localStorage.memberId);
        $scope.ws.onopen = function () {

            // Test if connected to Network
            testNetwork.test();

            // Test if Location services are on
            testGps.test();


            location.getLocation().then(function (position) {
                latitude = position.coords.latitude;
                longitude = position.coords.longitude;
                var request = '{"message":"{\\\"ID\\\":0,\\\"productID\\\":\\\"' + $localStorage.selectedProductID + '\\\",\\\"lat\\\": \\\"' + latitude + '\\\",\\\"lng\\\": \\\"' + longitude + '\\\"}","command":"memberPanic","sentFromID":"' + $localStorage.memberId + '","sentFromName":"' + $localStorage.phone + '","sentTo": "0" }';
                if (angular.isDefined(request)) {
                    $scope.ws.send(request);
                }
            });
        };

        $scope.ws.onerror = function (event) {
            var myError = event.data;
        };

        $scope.ws.onmessage = function (event) {
            var foo = event.data;
        };
    };
});

